VERIFICARE VERSIONE CEI E TRADUZIONE CATTOLICA IN INGLESE.

Apocalisse 3:14-22

14 All'angelo della Chiesa di Laodicèa scrivi:
Così parla l'Amen, il Testimone fedele e verace, il Principio della creazione di Dio: 15 Conosco le tue opere: tu non sei né freddo né caldo. Magari tu fossi freddo o caldo! 16 Ma poiché sei tiepido, non sei cioè né freddo né caldo, sto per vomitarti dalla mia bocca. 17 Tu dici: «Sono ricco, mi sono arricchito; non ho bisogno di nulla», ma non sai di essere un infelice, un miserabile, un povero, cieco e nudo. 18 Ti consiglio di comperare da me oro purificato dal fuoco per diventare ricco, vesti bianche per coprirti e nascondere la vergognosa tua nudità e collirio per ungerti gli occhi e ricuperare la vista. 19 Io tutti quelli che amo li rimprovero e li castigo. Mostrati dunque zelante e ravvediti. 20 Ecco, sto alla porta e busso. Se qualcuno ascolta la mia voce e mi apre la porta, io verrò da lui, cenerò con lui ed egli con me. 21 Il vincitore lo farò sedere presso di me, sul mio trono, come io ho vinto e mi sono assiso presso il Padre mio sul suo trono. 22 Chi ha orecchi, ascolti ciò che lo Spirito dice alle Chiese.
