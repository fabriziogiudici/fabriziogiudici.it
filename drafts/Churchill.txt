A noi vengono da ripetere le parole che pronunciò Winston Churchill al Mit di Boston in occasione del suo viaggio al termine della Seconda Guerra mondiale. Osannato come il salvatore della civiltà per aver sconfitto Hitler, dopo il discorso del rettore che prefigurava un futuro senza più guerre, dove il mondo avrebbe funzionato come un meccanismo perfetto, Churchill si alzò in piedi e pronunciò solo poche semplici parole: «Spero per allora di essere già morto».

Leggi di Più: La Svezia ha istituito il "ministero del futuro" | Tempi.it 
Follow us: @Tempi_it on Twitter | tempi.it on Facebook
