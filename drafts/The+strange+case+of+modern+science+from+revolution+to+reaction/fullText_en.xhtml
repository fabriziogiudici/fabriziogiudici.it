<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <p><span class="firstLetter">L</span>a scienza ha una caratteristica che la rende molto diversa da altre discipline
      umane: la capacità di auto-regolarsi, di convalidare sé stessa. È il <b>metodo sperimentale</b>, che prevede un
      meccanismo di verifica stringente: si formula una teoria, si fanno delle previsioni sul mondo reale, si verifica
      come si comporta il mondo reale; se il comportamento non è compatibile con le previsioni, la teoria non è valida.
      Se è compatibile, la teoria viene ritenuta valida fino a quando l'estensione del suo campo di applicazione la
      mette in crisi, perché iniziano a verificarsi previsioni sbagliate. In questo caso, si cerca di formulare una
      teoria più generale, in grado di passare il meccanismo di verifica. Questo processo è noto come <b><a href="http://it.wikipedia.org/wiki/Principio_di_falsificabilit%C3%A0">principio
          di falsificabilità</a></b>, definito da <a href="http://it.wikipedia.org/wiki/Karl_Popper">Karl Popper</a>. </p>
    <p>Quando la disciplina scientifica a cui stiamo pensando è anche formulabile in termini quantitativi (come la
      fisica o la chimica), le cose apparentemente sono più facilmente verificabili: dopotutto si tratta di manipolare
      formule, raccogliere dati e comparare numeri. In effetti, quando la scienza era giovane, scienziati e filosofi si
      fecero ingannare da questa apparente facilità. Pur comprendendo di essere appena all'inizio di un lungo cammino,
      avevano intuito le enormi potenzialità del metodo scientifico: l'universo poteva sì essere grande, praticamente
      tutto da scoprire, ma bastava avere pazienza; passo dopo passo, il metodo scientifico era una sorta di “pilota
      automatico” che avrebbe permesso di conoscere tutto il conoscibile. Di più, avrebbe anche fornito un nuovo modo di
      risolvere le controversie in altri ambiti umani; per esempio, il giovane Leibniz <a href="http://www.giutor.com/sdf/mod/capitolo%206/cap_6par_9.htm">sosteneva</a>
      nei suoi Scritti di Logica:</p>
    <blockquote>
      <p> Per ritornare all'espressione dei pensieri per mezzo di caratteri, sento che le controversie non finirebbero
        mai e che non si potrebbe mai imporre il silenzio alle sette, se non ci riportassimo dai ragionamenti complicati
        ai calcoli semplici, dai vocaboli di significato vago ed incerto ai caratteri determinati... Una volta fatto
        ciò, quando sorgeranno controversie, <b>non ci sarà maggior bisogno di discussione tra due filosofi di quanto
          ce ne sia tra due calcolatori</b>. Sarà sufficiente, infatti, che essi prendano la penna in mano, si siedano a
        tavolino, e si dicano reciprocamente (chiamato, se loro piace, un amico): <strong>calculemus</strong>.</p>
    </blockquote>
    <p> È un'idea comprensibile: l'esercizio delle libertà implica la necessità di compiere decisioni continuamente, le
      quali richiedono conoscenza (LINK AL POST PRECEDENTE); ma non appena si prendono due o più esseri umani, le
      libertà si scontrano e nascono dei conflitti, a volte perché la conoscenza è insufficiente o non basata su
      fondamenta comuni. L'uomo ha tradizionalmente risolto questi conflitti con la forza, oppure facendo riferimento a
      elites, commissioni di saggi o depositari della conoscenza per motivi trascendenti; purtroppo, nella maggioranza
      dei casi, questo approccio è stato solo un passaggio intermedio per arrivare comunque ad un'imposizione con la
      forza. Lo sforzo di Leibniz e di altri filosofi era quello di definire un processo decisionale oggettivo, cioè
      fuori dal dominio di singoli uomini o gruppi di potere, che potesse avere caratteristiche di universalità, senza
      ricorrere al trascendente, in modo da poter avere facilmente il consenso di tutti; il ragionamento razionale ed il
      calcolo sembravano essere proprio la soluzione cercata. Tanto che alla fine del '700 venne formulato il concetto
      di <strong><a href="http://ulisse.sissa.it/chiediAUlisse/domanda/2004/Ucau041110d001/">“scienza esatta”</a></strong>
      come:</p>
    <blockquote>
      <p> ... una scienza ... [che sappia] rispondere (almeno in linea di principio) a <strong>qualsiasi concepibile
          domanda all'interno del proprio ambito</strong>, secondo un <strong>metodo rigoroso</strong> e con risultati
        tali da essere in accordo coi fatti <strong>senza margini residui di incertezza</strong>, o quantomeno con <strong>margini
          ridottissimi ed esattamente quantificabili</strong>.</p>
    </blockquote>
    <p> e il concetto conobbe sempre crescente fortuna, grazie all'accelerazione esponenziale della scienza e della
      tecnologia; fino ad arrivare al positivismo a cavallo tra fine '800 e inizi del '900.</p>
    <p> Per la verità, la fine dell'800 e l'inizio del '900 furono caratterizzati da un momento di crisi: la scoperta
      che la velocità della luce nel vuoto è indipendente dal sistema di riferimento (celebre <a href="http://it.wikipedia.org/wiki/Esperimento_di_Michelson-Morley">l'esperimento
        di Michelson-Morley</a>), che mise in discussione la meccanica galieiana (crisi risolta da <a href="http://it.wikipedia.org/wiki/Albert_Einstein">Albert
        Einstein</a> con la <a href="http://it.wikipedia.org/wiki/Relativit%C3%A0_ristretta">relatività ristretta</a>).
      Ma è pur vero che la soluzione arrivò nel solco del metodo tradizionale: un esperimento mise in crisi una teoria,
      quindi venne formulata una teoria più ampia, la quale era in grado di produrre un numero di predizioni
      verificabili più ampia della precedente. La storia della scienza, d'altronde, mostra che essa non si evolve in
      modo uniforme: ci sono momenti “ordinari”, in cui essa procede linearmente, e momenti “straordinari”, di crisi
      delle certezze esistenti, che generalmente vengono risolte da vere e proprie rivoluzioni, come quella di Einstein.
      Questo è normale e opportuno: si può sostenere anzi che la scienza è di per sé rivoluzionaria (dopotutto è nata
      dalla rivoluzione copernicana) e i momenti di crisi, lungi dal mettere in discussione il metodo scientifico, sono
      proprio quelli più fecondi. </p>
    <p> Tuttavia, gli epistemologi <a href="http://it.wikipedia.org/wiki/Thomas_Kuhn">Thomas Kuhn</a> e <a href="http://it.wikipedia.org/wiki/Imre_Lakatos">Imre
        Lakatos</a> qualche decennio più tardi analizzarono la crisi di fine '800 rilevando che le cose non si erano
      svolte linearmente secondo il percorso del criterio di falsificazione di Popper. La meccanica pre-relativistica,
      in realtà, non aveva trovato un singolo, improvviso, evidente momento di difficoltà; nel tempo, per non
      contraddire una serie di sviluppi, aveva dovuto introdurre un concetto, quello dell'<a href="http://it.wikipedia.org/wiki/Etere_%28fisica%29">etere
        luminifero</a>, una sostanza che avrebbe dovuto possedere proprietà fisiche contraddittorie (per esempio
      pervadere tutto lo spazio senza tuttavia generare attrito). Insomma, la teoria precedente richiedeva una serie di
      <strong>ipotesi aggiuntive</strong> per “tenere in piedi la baracca” e perdeva lentamente la sua eleganza
      originale. Le previsioni formulate a partire dalla supposizione dell'esistenza dell'etere non venivano azzeccate,
      finché il citato esperimento di Michelson-Morley (che, curiosamente, fu una sorta di fallimento perchè avrebbe
      dovuto finalmente chiudere la diatriba sull'etere in senso positivo) non fu che la classica goccia che fa
      traboccare il vaso. Ma quando Einstein propose la relatività ristretta non ci fu un generale sospiro di sollievo
      perché era stata proposta una soluzione elegante al problema: viceversa <em>le opposizioni proseguirono</em> e fu
      necessario del tempo perché la nuova teoria venisse accettata. Nonostante le apparenze, anche gli scienziati
      posseggono una forte vena conservatrice. Insomma, bisognava prendere consapevolezza che il mondo, e
      conseguentemente la scienza, erano molto più complessi di quanto Leibniz supponesse; e che gli scienziati sono
      esseri umani come gli altri, magari dotati di un pensiero razionale più sviluppato della media, ma non agiscono
      come dei freddi calcolatori. </p>
    <p>Il concetto di “scienza esatta” fu mandato in pensione da successive scoperte del '900, come i <i><a href="http://it.wikipedia.org/wiki/Teoremi_di_incompletezza_di_G%C3%B6del">Teoremi
          di Incompletezza</a></i> di <a href="http://it.wikipedia.org/wiki/Kurt_G%C3%B6del">Kurt Gödel</a>, il <i><a
          href="http://it.wikipedia.org/wiki/Principio_di_indeterminazione_di_Heisenberg">Principio di Intederminazione</a></i>
      di <a href="http://it.wikipedia.org/wiki/Werner_Karl_Heisenberg">Werner Heisenberg</a>, la scoperta dell'<i><a href="http://it.wikipedia.org/wiki/Teorema_di_Turing">insolubilità
          del “problema dell'alt”</a></i> di <a href="http://it.wikipedia.org/wiki/Alan_Turing">Alan Turing</a> e, per
      finire, la scoperta del <i><a href="http://it.wikipedia.org/wiki/Teoria_del_caos">Caos Deterministico</a></i>
      (noto come “effetto farfalla”) di <a href="http://it.wikipedia.org/wiki/Edward_Norton_Lorenz">Edward Lorenz</a>.
      Oggi si classifica piuttosto in <i><a href="http://it.wikipedia.org/wiki/Scienza_dura">“scienze dure” e “scienze
          molli”</a></i> per distinguere tra le discipline maggiormente caratterizzate da approccio quantitativo e non;
      ma, anche per le prime, nessuno pensa che le dispute si possano risolvere con quell'ingenuo <em>calculemus</em>
      di Leibnitz. Infatti il criterio oggi usato in ambito scientifico è la cosiddetta <b><a href="http://it.wikipedia.org/wiki/Revisione_paritaria">revisione
          paritaria</a></b> (<strong>peer review</strong>): gli scienziati scrivono bozze di articoli per riviste o
      presentazioni per conferenze e le sottopongono a collegi di arbitri specifici per ogni ambito, che li esaminano e
      decidono se accettarli o rifiutarli (con vari gradi intermedi, come l'accettazione parziale condizionata ad
      approfondimenti o revisioni). In teoria non devono esserci gruppi privilegiati (da cui quel “paritario”) e il
      rinnovamento continuo dei collegi di revisori dovrebbe evitare interferenze estranee. Ma, oggettivamente, sarebbe
      illusorio crederlo. La comunità scientifica è inevitabilmente auto-referenziale: sono scienziati coloro che
      formano le nuove generazioni e approvano o disapprovano le pubblicazioni, contribuendo quindi a favorire o
      stroncare le carriere delle nuove leve, che successivamente diverranno i nuovi formatori e revisori; e così via.
      Rimane il faro del metodo sperimentale, che rende la scienza comunque diversa da filosofia, politica ed altre
      discipline umane; ma il contesto è talmente complesso che non c'è nessun pilota automatico, come la logica ed il
      calcolo a cui pensava Leibniz, che può garantire al cento per cento l'oggettività del processo. Il metodo
      sperimentale <em>non cammina sulle proprie gambe, ma su quelle degli scienziati e non può evitare la naturale
        imperfezione umana, che è l'anello debole della catena</em>. La soluzione sta nell'<b>etica professionale</b>
      degli scienziati; ma l'etica è una cosa a sé, sta fuori dalla scienza: ecco perché la scienza, da sola, non può
      salvare sé stessa dagli abusi. Ha torto quindi chi sostiene che la scienza ha in sé la soluzione ai propri
      problemi.</p>
    <p> Siamo quindi tornati alla casella di partenza, come nel gioco dell'oca: la decisione è di nuovo nelle mani di
      elites. Se oggi un novello Einstein pubblicasse una teoria “rivoluzionaria”, essa apparirebbe prima di tutto come
      articolo o presentazione per una conferenza. Probabilmente non verrebbe accettato subito da un collegio di
      arbitraggio, ma ci sarebbe un breve processo di approfondimento. Una volta pubblicato, verrebbe sottoposto a
      dibattito e confutazione. Si formerebbero vari “partiti” di favorevoli e contrari, contemporaneamente partirebbe
      la programmazione delle attività sperimentali per tentare una falsificazione (attività che potrebbe essere di
      medio o di lungo periodo a seconda della complessità e del costo degli esperimenti: si pensi che sono stati
      necessari cinquant'anni avere una prima conferma dell'esistenza del <a href="http://it.wikipedia.org/wiki/Bosone_di_Higgs">Bosone
        di Higgs</a>). Dopo un certo periodo di tempo, la teoria perderebbe popolarità, oppure verrebbe accettata nel
      cosidetto <strong>“<em>mainstream</em> scientifico”</strong>: ovvero, la maggioranza della comunità degli
      scienziati la riterrebbe valida. Si noti che il processo non è mai definitivo: il principio di falsificabilità di
      Popper sancisce che in ogni momento potrebbe essere sperimentata una predizione sbagliata che <em>potrebbe</em>
      rimettere tutto in discussione (si pensi a cosa sarebbe accaduto se l'esistenza dei neutrini più veloci della luce
      (LINK) fosse stata confermata).</p>
    <p> Questa complessità del processo scientifico moderno è poco compresa dall'opinione pubblica. Siccome oggi si
      comunica su tutto, è sempre più frequente che singoli articoli scientifici, almeno nei campi che hanno a che fare
      con la vita quotidiana, vengano immediatamente citati dalla stampa generalista: a volte senza commento, altre con
      commenti strumentali. La percezione di potenza che scienza e tecnologia esercitano nei confronti dell'uomo della
      strada fa sì che esso pensi che ogni nuova pubblicazione sia immediatamente data per acquisita nel <em>mainstream</em>.
      Questo porta a conseguenze grottesche e paradossali: per fare un esempio banale, basterebbe raccogliere gli
      articoli di giornale che negli ultimi trent'anni hanno promosso o condannato l'uso dell'olio di semi per la
      frittura, oppure magnificato o ridimensionato il ruolo della vitamina C come prevenzione di certe malattie per
      ottenere un'incredibile sequenza di asserzioni contraddittorie. L'ultimo episodio della saga, letto pochi giorni
      fa, è relativo ad una ricerca “rivoluzionaria” che denuncerebbe i rischi per il sistema cardio-circolatorio
      causati da un eccesso di grassi omega-3; questo dopo un decennio di campagne mirate ad un maggior consumo di
      pesce, per non parlare dell'industria dell'alimentazione che ha introdotto sempre più cibi arricchiti con queste
      sostanze. Questa comunicazione scientifica privata del contesto, oppure sfuggita troppo presto dai consessi
      professionali, produce effetti paradossali: la nascita di gruppi di opinione contrapposti, entrambi però
      convintissimi delle proprie idee in quanto basate sulla presunta oggettività scientifica (evidentemente
      l'esistenza di “oggettività contrastanti” a molti non suggerisce che ci sia un problema); oppure un rigetto della
      scienza da parte di una porzione dell'opinione pubblica.</p>
    <p> Fin qui stiamo parlando della sfera delle decisioni private: dopotutto se consumiamo più o meno pesce od olio di
      semi non se ne farà un dramma. Ma ne siamo sicuri? Perché negli ultimi decenni, a livello planetario, si tende
      sempre più a “educare” l'opinione pubblica verso modelli virtuosi di comportamento, formulati “per il benessere di
      tutti” e basati su una supposta oggettività scientifica. Mangiare più o meno pesce, quindi, non è più una
      decisione totalmente personale: organizzazioni private prima, pubbliche poi, sino ad arrivare agli Stati e agli
      organismi inter-statali come l'ONU e le sue emanazioni, investono sempre più in massicce campagne di
      “sensibilizzazione” che, attraverso il condizionamento dell'opinione pubblica, arrivano ad influenzare
      pesantemente gli investimenti, lo sfruttamento delle risorse e gli equilibri geo-politici. Lasciamo pure stare la
      (forse) innocua diatriba sull'olio o sul pesce; pensiamo piuttosto alle sempre più numerose “politiche globali”,
      incluso visioni antropologiche sulla “giusta” conformazione della società umana e sulla definizione dei diritti
      “naturali”, propugnate non su basi democratiche, ma su supposte basi di oggettività scientifica. Se poi prendiamo
      in considerazione anche il fenomeno della <strong>“megascienza”</strong> (<strong>big science</strong>), cioè
      l'intreccio sempre più intricato, a partire dal secondo dopoguerra, tra scienza, industria, investitori pubblici e
      privati, a causa delle quantità di finanziamenti sempre più elevate necessarie in molti campi e la banale
      considerazione che ogni finanziamento richiede, direttamente o indirettamente, l'avallo dell'opinione pubblica
      (che deve accettare la destinazione delle proprie tasse o il costo di certi beni o servizi), è lecito ipotizzare
      l'esistenza di parecchi conflitti di interessi che inevitabilmente influenzano il processo scientifico. </p>
    <p> Probabilmente l'episodio più rilevante è quello del <a href="http://it.wikipedia.org/wiki/Riscaldamento_globale"><b>Riscaldamento
          Globale Antropogenico</b></a> (AGW), teoria che nell'ultimo decennio ha influenzato tantissimi aspetti delle
      politiche mondiali, spostando masse enormi di capitali. Lo scandalo del cosiddetto <em><a href="http://it.wikipedia.org/wiki/Climategate">ClimateGate</a></em>
      ha dimostrato grosse lacune nell'etica professionale, al punto da avere “partiti” di scienziati che hanno tentato
      per anni di boicottare pubblicazioni “scettiche” e di estromettere scienziati “negazionisti” dal dibattito, per
      poter contemporaneamente affermare che all'interno della comunità scientifica ormai esisteva un consenso
      consolidato. Eppure, a distanza di qualche anno, si sono evidenziati errori di previsione: la temperatura media
      del pianeta ha smesso di crescere negli ultimi 12-15 anni (LINK), contraddicendo tutti i modelli climatici alla
      base di quel modello; tanto che la stessa forma di comunicazione da parte del “partito AGW” ultimamente ha preso
      toni un po' più moderati (tranne che per i gruppi dei più realisti del re... ATTENZIONE, SPIEGA MEGLIO, VEDI BOZZE ULTIMO RAPPORTO IPCC). Senza dover prendere posizioni pro
      o contro la teoria dell'AGW, oggi si può neutramente dire che si è ben lontani dall'avere una teoria consolidata:
      nonostante ciò l'influenza sulle politiche globali è notevole.<br />
    </p>
    <p> E questo ha in qualche modo chiuso il cerchio. La scienza, evocata ai suoi albori come criterio di conoscenza e
      di convivenza in grado di spostare la responsabilità delle decisioni dai gruppi umani più forti ad una anelata
      oggettività, oggi è - almeno in parte - strumento dei gruppi politici più forti, fino a diventare, per alcuni, la
      base di una nuova etica da imporre a tutti. Con un ennesimo paradosso: da <em>rivoluzione</em> contro
      l'assolutismo dei blocchi di potere del passato, accusati di basarsi su assunzioni trascendenti non condivisibili
      da tutti, essa rischia di diventare il braccio armato di una <em>reazione</em> neo-assolutista, fondata su una
      oggettività che spesso è solo asserita strumentalmente e quindi non è migliore di ciò che ha voluto sostituire. </p>
    <p>Si può osservare che è un destino comune a tutte le rivoluzioni quello di favorire un gruppo di rivoluzionari che
      passa da minoranza a maggioranza e poi perpetra gli stessi errori che ha condannato: il Periodo del Terrore che fece seguito alla Rivoluzione Francese è emblematico.
È d'altronde questo il senso
      dell'allegoria del <a href="http://it.wikipedia.org/wiki/Ciclo_delle_Fondazioni">Ciclo delle Fondazioni</a> di <a
        href="http://it.wikipedia.org/wiki/Isaac_Asimov">Isaac Asimov</a><a href="http://it.wikipedia.org/wiki/Ciclo_delle_Fondazioni"></a>:
      la Prima Fondazione, autrice della rivoluzione culturale che doveva portare pace ed armonia nell'Impero, dopo una
      prima fase di successo finisce succube di un potere dittatoriale che ne fa il proprio strumento di dominio; fato
      previsto dallo stesso fondatore, che sin dall'inizio aveva segretamente istituito una <a href="http://it.wikipedia.org/wiki/Seconda_Fondazione">Seconda
        Fondazione</a> come antidoto. E si noti che mentre la Prima Fondazione era forte nelle scienze fisiche, la
      Seconda era composta da <em>mentalics</em>, esperti nella comprensione delle emozioni umane: come dire che non è
      la scienza che salverà l'uomo, né salverà sé stessa con qualche automatismo,  ma è compito dell'uomo, un uomo a tutto tondo, salvare la scienza. </p>
  </body>
</html>
