Una volta Reims era dominata dalla sua cattedrale; il visitatore che si recava in città l'avrebbe vista da lontano, adagiata su una serie di dolci colline, con lo splendido edificio gotico che svettava insieme alle sottili guglie di altri edifici religiosi. Era certamente anche il segno del condizionamento del pensiero religioso sulla vita dei cittadini, in passato imposto a forza. Poi sono giustamente arrivate la libertà e la modernità.  
Oggi chi arriva dall'autostrada vede prima di tutto le brutte costruzioni della periferia e i grattacieli; del centro storico rimane poco, a parte i residui gioielli come la Cattedrale, il Palazzo di Tau o l'Abbazia di Saint-Remi. Sono però dispersi in una città anonima, bruttoccia, dove il tessuto del centro storico è sostanzialmente sparito. Molte città europee hanno fatto la stessa fine, anche se fortunatamente altre conservano ancora la loro bellezza originale. E' una grande metafora: abbiamo la libertà di scegliere, ma la scegliamo male; ci dimentichiamo di Dio e come naturale conseguenza perdiamo di vista la bellezza, ci circondiamo di cose brutte - nel migliore dei casi, anonime.


https://www.google.it/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&docid=D0Y4Oyeup3q2qM&tbnid=89zEWYU79iBFnM:&ved=0CAUQjRw&url=http%3A%2F%2Fpierreplantaz.free.fr%2F%3Fcat%3D6&ei=zssyUuC6A6i_0QX_woGgCA&bvm=bv.52164340,d.ZGU&psig=AFQjCNFhmGVnvbVnsSd3GE6SBP5O3mm5Zg&ust=1379147011935868


http://radiospada.org/2014/05/le-alte-torri-costruite-sul-vuoto/
http://www.lemonde.fr/idees/article/2008/07/09/la-symbolique-des-gratte-ciel-par-denis-dessus-isabelle-coste-et-david-orbach_1068104_3232.html
