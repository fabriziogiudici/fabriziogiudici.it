<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <p><span class="firstLetter">I</span>n my profession as a computer engineer I often attend and actively participate
      in conferences. In such events a speaker is supposed to have a relatively short time slot (typically thirty/forty
      minutes) to present a product, a technology, a process that are supposed to be useful for some purpose. Computer
      science is complex and you need quite a few good communicating skills in order to condensate the key concepts in
      such a short time. You can't explain everything; your primary goal is to capture the listener's attention, get him
      curious about the fundamentals so, at a later time, he will probably go and learn more by himself. </p>
    <p>An efficient way to accomplish the task is to end the presentation with a slide <em>“If you can only remember
        one thing...”</em> where <strong>a single key concept, that has been previously illustrated, is recalled</strong>.
      It is the thing that will have more chances to stay in the listener's mind. It is important, because your
      presentation is just one among many others in mutual competition: <em>you should perform better than others</em>.
      It is almost always impossible to find a single key concept that is sufficient to fully describe a complex matter
      - I mean that it's not possible to deduce a complete description of a complex thing out of a single premise. So,
      you have to make a trade-off. </p>
    <div class="float-center"> <img src="$mediaLink(relativePath='/Conference_of_Engineers_at_the_Menai_Straits_Preparatory_to_Floating_one_of_the_Tubes_of_the_Britannia_Bridge_by_John_Lucas.jpg')$" />
      <p class="caption">Conference of Engineers at the Menai Straits Preparatory to Floating one of the Tubes of the
        Britannia Bridge (1868).<br />
        Engraving by James Scott, after John Lucas, portrait group of Robert Stephenson, Isambard Kingdom Brunel, and
        other engineers, consulting over the Menai Bridge.<br />
        Image courtesy of Wikipedia. </p>
    </div>
    <p>What to put in that critical last slide? Perhaps something that is easy to understand? But pay attention: <strong>usually
        the easy things aren't those which make the difference</strong>. You can just say <em>“This stuff will solve
        your problems”</em>, but recall the competition thing: every other presenter could say the same. You are not
      doing better than them. Thus usually it's better <strong>to focus on some good things that you have and others
        don't</strong>. And if your idea is really better than others, those peculiar things are precisely its added
      value, the things that effectively make it “better”. </p>
    <p>It might happen that the added value is not easy to explain; you have to work it out. Nobody said that life is
      easy.</p>
    <p>This job is a sort of “secular evangelisation”; actually, there exists a professional role named as <a href="http://en.wikipedia.org/wiki/Technology_evangelist">“technology
        evangelist”</a>.</p>
    <p>As Catholic faithfuls, we are called to evangelising others - in the XXI century it is actually one of our main
      duties - and we are in a similar situation as the presenter in a conference. In fact, unlike the past when
      Catholicism was the dominant culture, now we have to share the listener's attention with many other things, world
      views or other religions. </p>
    <p>So let's pretend that we have to describe our Catholic Church to someone who knows little or nothing about her:
      what should we put in that last slide <em>“If you can only remember one thing…”</em>? For instance: <em>“The
        Catholic Church helps the poor”</em>? <em>“She works for peace in the world”</em>? <em>“She defends the
        natural family”</em>? <em>“She fights for the right to life of the unborn”</em>? All of these are good things
      that the (true) Catholic Church actually does, and that's fine. But these can't be the single fundamental things
      that describe her core business. In fact, there are a number of secular ONGs that do exactly the same.
      Furthermore, some things that the Church does, as defending the unborn babies, aren't considered by many a good or
      an important thing; instead they are unpopular in the western culture. If you stop yourself at this level, you
      fatally have to “split” the Church in portions and unavoidably you'll be tempted to present only the most popular
      ones. But the more popular, the more competition: you won't capture many listeners, and you could easily lose most
      of them in favour of other world views or religions which promote the same points. Failure.</p>
    <p>Let's try to improve. A religion is about God, so He should be at the top of the list. So: <em>“The Catholic
        Church tells about God”</em>; <em>“She tells about God's laws of good and evil”</em>; <em>“She spreads the
        Gospel”</em>; <em>“She talks about God's love for mankind”</em>. Much better, indeed. Also because, from a
      knowledge point of view, from God and His Word you can derive that the Church does things such as “helping the
      poor”. But it's not enough; in fact there are many other religions, with their organisations, that talk about God
      - in their way of course. Honestly, the Gospel says that the Catholic way to God is hard and goes through a <em>“narrow
        gate”</em> <a href="http://www.vatican.va/archive/ENG0839/_PWW.HTM#GOSP.LUK.13.24">[Luke 13, 24]</a>. Many
      other religions take a different, more relaxed and appealing approach, even though untrue. Again, you're going to
      lose to the competition in this field. Not to mention that the word “love”, in contemporary culture, means
      everything (“love is love”), <em>thus it means nothing</em>. So, it's not clear what is that God's love. Failure
      again.</p>
    <p>What about: <em>“The Catholic Church repeats in the Mass the Jesus' Eucharistic Sacrifice on the Cross and
        delivers the Holy Communion to people”</em>? This is something that only the Catholic Church does (along with
      the Eastern Orthodox Church). It is an extraordinary, hard to believe and even mysterious thing: God becomes
      flesh, takes the appearance of bread and wine and can be consumed and assimilated by a human being. This is the
      true core business of the Catholic Church, the very reason for her existence, the thing that makes her different
      than any other religion or world view: while others are just “ethic systems”, <strong>Catholicism is about the
        meeting between God and man</strong>. And it happens in the Communion. There is a catholic ethic system, which
      is also fundamental (<em>“Whoever has my commandments and observes them is the one who loves me”</em> <a href="http://www.vatican.va/archive/ENG0839/_PXM.HTM#GOSP.JOH.14.21">[John
        14, 21]</a>), but it derives from the meeting between God and man. The Eucharist is the thing that people should
      be made curious about. San Pio da Pietralcina used to say: <em>“If people understood what the Mass is about, the
        police would be needed in front of the churches to rule the crowds.”</em>.</p>
    <p>Now, one might argue that the Eucharistic Sacrifice is very hard to explain to those who haven't received a
      catholic education. It's true, because - just to say a few things - involves the mystery of the Holy Trinity and
      the one of the Dual Nature of Jesus Christ; and some other Sacraments. But do you recall what is the last slide
      for? First get people interested, don't pretend to explain everything. If you do it right, if you capture the
      listener's interest, there will be some further time for the details. There's no alternate, easier path. If you
      prefer the easier path, such as “helping the poor”, failure is unavoidable for the reasons I explained above. It's
      not easy, but nobody said that life is easy. </p>
    <p>The topic about how to create interest in the Holy Eucharist deserves a further post. In the meantime, one might
      ask whether the Catholic Church is following the right strategy. It doesn't seem so. For instance, every year,
      when approaching tax declaration time, the Italian Episcopal Conference (CEI) broadcasts a set of advertisements
      for promoting the donation of a fraction of the income tax return, as allowed by the Italian law. Three quarters
      of the collected money are correctly spent for supporting of the clergy, which is the base requirement for
      celebrating the Mass and the Eucharistic Sacrifice. But the ads only focus on philanthropic activities: no mention
      of Jesus at all. They say <em>“We're changing people's life”</em>, but it appears that it happens by finding
      people a job, a house, a friend... not by having them meet Christ in the Eucharist. This is hardly different than
      ads made by other Christian churches which don't celebrate the Eucharistic Sacrifice. So, if a non-Catholic sees
      the advertisement, why should he be attracted by the Catholic Church? And if a Catholic sees the advertisement,
      won't he be inducted to think that philantropy, not Eucharist, is the mission of the Church? <strong>This is not
        grabbing the attention of the non-Catholic and, on the other hand, it is confusing the Catholic</strong>. The
      proof lies with the fact that <strong>less and less Catholics attend the Mass and understand Real Presence</strong>,
      as reported by a number of studies.</p>
    <blockquote>
      <p>By far not all Catholics believe in the Real Presence of Christ in the consecrated host. One can see this fact
        already in the way many – even priests – pass the tabernacle without genuflection. (<a href="https://www.lifesitenews.com/news/cardinal-brandmueller-advocates-for-changing-catholic-teaching-on-marriage">Cardinal
          Brandmüller</a>)</p>
    </blockquote>
    <p>An advertisement is pretty much a “last slide”. So, on the above premises, I'd expect to see something like <em>“Donate
        your taxes to support the Catholic Church in celebrating the Mass and delivering the Holy Communion to people”</em>.
      And, of course, nothing prevents from adding <em>“And she helps the poor too”</em>. </p>
    <p>But things aren't like that. Why? Of course it's not only a problem of advertisements, they are just an example.
      Perhaps is this the biggest communication problem behind the current Catholic Church crisis?</p>
  </body>
</html>
