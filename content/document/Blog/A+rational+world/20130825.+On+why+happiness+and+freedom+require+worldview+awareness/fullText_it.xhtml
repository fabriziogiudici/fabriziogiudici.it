<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <div class="right-note"><em> <a href="$nodeLink(relativePath='/Blog', contentRelativePath='/Blog/A rational world/20130825. On why happiness and freedom require worldview awareness', language='en')$">English
          version available.</a></em></div>
    <p><span class="firstLetter">C</span>redo che <em>ogni</em> uomo moderno non possa esimersi dal formulare una sua <em>visione
        del mondo</em>, una presa di coscienza che gli dia consapevolezza delle proprie decisioni. Se infatti l'uomo
      moderno è, almeno in molte parti del pianeta, <em>libero</em> nel pensiero, nell'azione e anche libero dai
      bisogni primari, l'esercizio della libertà si traduce nel fatto che gran parte della sua vita è una catena di
      decisioni. Quanto più queste decisioni sono consapevoli, frutto di considerazioni coerenti formulate a partire dai
      propri convincimenti personali, in contrasto con l'accettazione acritica di posizioni proposte o imposte da
      soggetti terzi, tanto più questo uomo è libero. </p>
    <div class="float-top-right"><img src="$mediaLink(relativePath='/Caspar_David_Friedrich_032_(The_wanderer_above_the_sea_of_fog).jpg')$" />
      <p class="caption">“Viandante sul mare di nebbia”, Caspar David Friedrich (1818).<br />
        Hamburger Kunsthalle, Amburgo. Immagine da Wikipedia.</p>
    </div>
    <p> Ho scritto “ogni” perché non è un tema che riguarda solo gli intellettuali o i filosofi: <em>è una questione
        pratica</em>, della vita di tutti i giorni, che ci coinvolge tutti. In passato, comprensibilmente, riflettere
      sulla propria visione del mondo era un privilegio per pochi, in quanto la maggioranza dell'umanità viveva tutta la
      propria esistenza nell'ignoranza, sotto l'oppressione di tiranni, in balìa di malattie anche banali, carestie e
      guerre; aveva a malapena il tempo per pensare a come sopravvivere. Oggi, rotte tutte queste catene, non abbiamo
      più scuse: il mancato esercizio della propria libertà nella sua forma più piena è sostanzialmente un rinnegare la
      propria natura umana.</p>
    <p> La libertà oggi viene interpretata con una concezione che abbiamo ereditato dall'epoca dei Lumi, ovvero dalla <a
        href="http://it.wikipedia.org/wiki/Dichiarazione_di_indipendenza_degli_Stati_Uniti_d'America">Dichiarazione di
        indipendenza degli Stati Uniti d'America</a> e dalla successiva <a href="http://it.wikipedia.org/wiki/Dichiarazione_dei_diritti_dell'uomo_e_del_cittadino">Dichiarazione
        dei Diritti dell'Uomo e del Cittadino</a> del 1793, da cui poi sono discese molte Costituzioni europee moderne.
      È da questi documenti che provengono due definizioni molto popolari: </p>
    <blockquote>
      <p> We hold these truths to be self-evident, that all men are created equal, that they are endowed by their
        Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.</p>
      <p> Riteniamo queste verità di per sé stesse evidenti, che tutti gli uomini sono creati uguali, che essi sono
        dotati dal loro Creatore di alcuni Diritti inalienabili, che fra questi sono la Vita, la Libertà e la ricerca
        della Felicità.</p>
    </blockquote>
    <blockquote>
      <p> La liberté est le pouvoir qui appartient à l’homme de faire tout ce qui ne nuit pas aux droits d’autrui; elle
        a pour principe la nature; pour règle la justice; pour sauvegarde la loi; sa limite morale est dans cette
        maxime: Ne fais pas à un autre ce que tu ne veux pas qu’il te soit fait.</p>
      <p> La libertà è il potere che appartiene all'uomo di fare tutto quello che non nuoce ai diritti dell'altro; essa
        ha per principio la natura; per regola la giustizia; per salvaguardia la legge; il suo limite morale è descritto
        da questa massima: Non fare all'altro quello che tu non vuoi venga fatto a te.</p>
    </blockquote>
    <p> La prima formulazione asserisce quello che ogni uomo sente naturale, l'aspirazione alla felicità che viene
      ricercata mediante libere scelte. La seconda formulazione pone le basi per regolare la libertà dell'individuo in
      relazione con altre entità della sfera socio-politica, perché vengano definite e rispettate regole di pacifica
      convivenza. Una volta che però è stato delimitato il nostro perimetro d'azione, che dev'essere il più ampio
      possibile, come ci muoviamo all'interno di esso (<em>libertà come autodeterminazione</em>)? E dal momento che,
      vivendo in società liberal-democratiche, abbiamo la possibilità di intervenire sulla formulazione delle regole di
      convivenza, in base a cosa ci orientiamo (<em>libertà come partecipazione</em>)?</p>
    <p>Quello che ci serve è la conoscenza, che è alla base di ogni scelta. In altre parole, l'esercizio della libertà
      non può prescindere dalla <em>capacità di capire</em> cosa vogliamo e quali decisioni ci portano veramente al
      fine che ci siamo determinati di raggiungere; tenendo presente che certe scelte improvvisate possono invece
      portare conseguenze opposte a quelle attese, oppure ad un bene minore di quello desiderato. Tutto questo perché
      viviamo immersi in un mondo che non comandiamo, ma che è regolato da certe leggi di causa ed effetto che sono
      fuori da noi (e se si crede nell'esistenza di cose non deterministiche, certe questioni sono ancora più fuori dal
      nostro controllo).</p>
    <p> Per questo, <em>l'esercizio della propria libertà necessita di una ragionevole visione del mondo</em>. Questo
      esercizio richiede tempo e sforzo conoscitivo, in altre parole l'impegno di una parte della propria vita; è
      certamente un prezzo da pagare, ma l'alternativa è inevitabile: non essere padroni della propria vita, “farsi
      vivere” da altri attori (altre persone che possono esercitare un'influenza sulla nostra vita, mode, ideologie,
      partiti politici, poteri economico-finanziari); tra questi attori potrebbero esserci i nostri impulsi irrazionali
      e, anche se l'umanità ha una varietà di opinioni sul fatto che tali impulsi vadano più o meno assecondati,
      contrastati o controllati, penso sia di per sé evidente che agire <em>esclusivamente ed inconsapevolmente</em> in
      funzione dei propri impulsi sia tipico degli animali, non degli esseri umani.<br />
    </p>
    <p>C'è una parola tedesca, Weltanschauung, usata come “termine tecnico” in filosofia, che descrive pienamente il
      concetto di “visione del mondo” a cui mi sto riferendo, tenendo presente sia gli aspetti deterministici che
      escatologici (cioè dei rapporti causa-effetto e delle finalità, per chi ci crede); in modo non separabile dal
      fatto che l'uomo si colloca nel mondo e con esso interagisce; che il processo conoscitivo si avvale di vari
      strumenti, sia razionali che irrazionali (come l'intuito).</p>
    <blockquote>
      <p> <b><a href="http://www.treccani.it/enciclopedia/weltanschauung_(Dizionario_di_filosofia)/">Weltanschauung</a></b>
        Termine ted. («visione, intuizione [Anschauung] del mondo [Welt]»). Concezione della vita, del mondo; modo in
        cui singoli individui o gruppi sociali considerano l’esistenza e i fini del mondo e la posizione dell’uomo in
        esso (Dizionario di Filosofia Treccani, 2009).</p>
    </blockquote>
    <p>Una Weltanschauung non è una collezione disordinata di opinioni, ma un “posizionamento” ragionato in certi
      settori, collegato alle nostre credenze, idee politiche, gusti, passioni. Perché credo in questa cosa? Perché
      faccio quest'altra? Perché ritengo che questo e quest'altro siano giusto, o sbagliato, o opportuno, o inopportuno?
      Parte di queste posizioni sono - o dovrebbero - essere razionali. Altre sono irrazionali ed è normale che lo
      siano: l'essere umano non è solamente razionale. Essere in grado di stabilire quali scelte appartengano alla sfera
      irrazionale e quali a quella razionale è un buon punto di partenza per definire una propria visione del mondo. A
      volte certe posizioni irrazionali possono esprimere un pregiudizio, che può essere affrontato mediante
      razionalizzazione. Alternativamente, possiamo renderci conto che, grazie alla nostra accresciuta conoscenza di
      qualcosa, la razionalizzazione di un nostro convincimento non è più soddisfacente. Forse è il caso di ragionarci
      sopra, trovare altre spiegazioni, oppure ... cambiare idea. Nella vita è inevitabile cambiare idea su certe cose;
      alle volte i cambiamenti avvengono sottotraccia, così lentamente che non ci rendiamo conto di essere passati
      dall'altra parte del fiume, mentre siamo convinti di essere ancora sulla riva di partenza. Non prendere
      consapevolezza delle contraddizioni vuol dire prima di tutto mentire a sé stessi, creare un'immagine di sé che non
      coincide con la sostanza, il che poi implica mentire anche agli altri; questo spesso porta a scelte errate che poi
      presentano il conto a distanza di anni.</p>
    <div class="digression">
      <p>Io, oggi, sono pienamente soddisfatto della mia professione di ingegnere informatico. Con il passare del tempo
        sto espandendo i miei interessi a discipline diverse, come le arti figurative, la musica e la letteratura, ma
        ciò è pertinente al mio tempo libero. Dal punto di vista del lavoro, il mestiere che ho scelto è l'unico che può
        permettermi di guadagnarmi da vivere, da cui poi ovviamente discende tutto il resto. La mia predisposizione si è
        evidenziata sin da bambino; prima con un interesse non differenziato tra scienze (fisica, chimica, matematica) e
        tecnologia (informatica), che successivamente si è decisamente indirizzato verso quest'ultima. In modo
        assolutamente opportuno, perché l'approfondimento del calcolo matematico, passando attraverso livelli di
        conoscenza sempre più completi, mi ha fatto capire che non è una materia che mi piace e potrei dominare fino al
        punto di basarci il mio lavoro. Questo mi ha permesso di escludere la fisica dalla mia scelta per l'università:
        se avessi scelto diversamente, sarei oggi un fisico piuttosto scarso. Inoltre, c'è stato un periodo di alcuni
        anni in cui l'arrivo di un gatto in famiglia mi ha fatto appassionare agli animali: ricordo perfettamente che
        all'epoca, a chi mi chiedeva cosa volevo fare da grande, rispondevo convintamente il veterinario. Fortunamente
        ho razionalizzato anche in quel caso: le mie capacità si accordano pessimamente con le decisioni da compiere in
        tempo reale (come per esempio quelle necessarie in chirurgia) e ho una certa avversione per tutto ciò che ha che
        fare con le malattie, in particolare quelle contagiose. Non avrei mai potuto svolgere quel mestiere. La capacità
        di razionalizzare, di capire come ero fatto, quali erano le mie capacità e i miei limiti, quello che mi piaceva
        e quello che non mi piaceva, la comprensione delle discipline come sono realmente e non come le idealizzavo, mi
        hanno permesso di evitare pregiudizi ed influssi esterni; l'esercizio pieno della mia libertà di scelta, basata
        sulla consapevolezza, mi ha consentito di evitare scelte sbagliate che mi avrebbero causato gravi
        insoddisfazioni.<br />
      </p>
    </div>
    <p> Compiuta una prima analisi a grandi linee, la razionalità può aiutarci a capire se alcune nostre posizioni sono
      tra loro incompatibili. In altre parole, quanto siamo <em>coerenti</em> con noi stessi? Non è coerente
      professarsi intransigenti contro la caccia e mangiare bistecche di cinghiale. Possiamo anche studiare quanto le
      nostre posizioni siano realistiche o illusorie. Viviamo in un mondo di “confusione semantica”, cioè nonostante
      l'aumentato grado di alfabetizzazione e di acculturazione sempre più spesso non diamo alle parole il giusto
      significato, a volte addirittura le usiamo in modo completamente improprio. A causa di questo deficit di
      comprensione rischiamo di <em>illuderci</em> di credere in certe cose, o di agire secondo certi criteri, mentre
      invece la sostanza è in totale dissonanza con la forma. Se amiamo la natura, siamo animalisti o ambientalisti? O
      entrambe le cose? Ci sono scelte ed azioni che sono compatibili con una di queste cose e contrarie all'altra;
      eppure molte persone non hanno ben chiara questa distinzione.<br />
    </p>
    <p> Fare periodicamente i conti con la propria visione del mondo, tenerla aggiornata, verificare la coerenza delle
      proprie posizioni con essa; sono tutte cose fondamentali per essere soddisfatti di sé e vivere pienamente la
      propria libertà. E ovviamente, alla base di tutto, è fondamentale far sì che questa visione del mondo sia la più
      chiara possibile.</p>
  </body>
</html>
