<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <p><span class="firstLetter">O</span>gnuno di noi ha delle cose care, a cui dà tanto valore da ritenerle essenziali,
      sacre. Per me una di queste cose essenziali è la poesia. La poesia è la forma di comunicazione verbale più
      sintetica in grado di cogliere la Bellezza intorno e dentro noi. Avendo la Bellezza origine divina, la poesia è
      anche una porta che apre un passaggio verso Dio. Non ha regole, la poesia: può materializzarsi nell'eloquio
      forbito e geometricamente elegante delle terzine della Divina Commedia di Dante, o nella metrica libera di
      Pascoli, fino all'estrema sintesi di Ungaretti.</p>
    <div class="float-top-right"> <img src="$mediaLink(relativePath='/William-Adolphe_Bouguereau_(1825-1905)_-_Inspiration_(1898).jpg')$" />
      <p class="caption">William-Adolphe Bouguereau, Ispirazione (1898)<br />
        Collezione privata.<br />
        Immagine gentile concessione di Wikipedia. </p>
    </div>
    <p>È doloroso constatare quanto poco la poesia sia apprezzata oggi, nonostante le apparenze dicano il contrario. Non
      assistiamo infatti alla sua negazione esplicita, ma allo svuotamento del suo significato, seguendo quel processo
      di manipolazione semantica della lingua che Orwell aveva sapientemente previsto. Il vero nemico della poesia non è
      quindi la sua mancanza - che lascerebbe un vuoto di cui quasi tutti si renderebbero conto, spingendoli ad una
      ricerca - ma la <em>banalità</em>, l'apparente magniloquenza senza fondamento, che può dare un falso senso di
      riempimento di quel vuoto. Ecco, oggi tutto è poesia, tutti sono poeti, tutti capiscono e amano la poesia, ma nel
      migliore dei casi non si è capaci di distinguere un cantautore da un poeta; nei casi più comuni, si arriva a
      definire poesia il gesto di un calciatore che ha scartato l'avversario e ha segnato, oppure quel particolare
      piatto servito da uno <em>chef</em> “stellato” magnificato in quel programma televisivo alla moda. Se mettete un
      lucchetto su una ringhiera, o scarabocchiate su un muro di un famoso vicolo a Verona, siete poeti. <em>Todos
        caballeros</em>. Se tutto è poesia, anche il banale, cioé l'esatto opposto della poesia, allora niente è poesia.
    </p>
    <p>Concausa importante di questo fenomeno - la cui radice primaria è la mancanza di formazione e di educazione alla
      fruizione del Bello - è il <em>conformismo auto-gratificante</em> per cui masse di persone vanno a rimorchio di
      un'idea “di successo”, tentando di appropriarsi di una briciola di quel successo. Se elogio Tizio, che è
      apprezzato e famoso, mi illudo di ricevere un po' della sua luce riflessa; se poi lo elogio superlativamente,
      ripetendo frasi fatte anch'esse di successo (per cui il calciatore, lo <em>chef</em> o l'attore diventano poeti),
      posso pure darmi un'aria da grande critico d'arte. Guardate i forum di fotografia: a meno che non siano
      partecipati da pochi intimi e competenti, nelle critiche è ormai quasi impossibile trovare giudizi equilibrati,
      non superficiali; si è sommersi da ripetitivi e scontati “meraviglioso”, “poetico”, “sembra un quadro” (frase di
      cui nessuno sa il significato concreto), ovviamente conditi da uno sproposito di punti esclamativi e <em>smiley</em>.
      Purtroppo temo che anche il successo di musei e mostre sia in parte dovuto più alla volontà di poter dire “io ci
      sono stato” che non al semplice desiderio di godere ed interiorizzare la bellezza in esposizione. È tutto un
      grande <em>selfie</em>, anche se non necessariamente si esprime in una foto, ed è un sintomo della dittatura
      dell'io che caratterizza questo periodo storico. Ancora più subdola delle precedenti ideologie novecentesche,
      lascia credere alle sue vittime di essere veramente libere e padrone di sé, mentre sono soggette ad una nuova
      forma di manipolazione. Infatti il fenomeno è ampiamente sfruttato a vantaggio di certi personaggi scaltri: per
      esempio, politici da quattro soldi scrivono biografie di grandi statisti del passato - senza peraltro aggiungere
      contenuti originali -, mettono su un ciclo di conferenze e, <em>voila</em>, eccoli splendere come la Luna che
      riflette i raggi del Sole. Si aprono praterie immense per editori ed autori che riescono a vendere in grande
      quantità i propri prodotti di scarsa qualità: cosa è bello e cosa è addirittura poetico, in definitiva, lo decide
      il loro ufficio del <em>marketing</em>, ben inserito nel <em>mainstream</em> mediatico.</p>
    <p>In Italia abbiamo un eccellente esempio del fenomeno pseudo-poetico: Roberto Benigni. Attore comico sin dalle
      origini, caratterizzato dall'atteggiamento graffiante e anarchico, con qualche problema di censura, il vero apice
      della sua carriera fu nella co-interpretazione di <a href="http://it.wikipedia.org/wiki/Non_ci_resta_che_piangere">“Non
        ci resta che piangere”</a> insieme a Massimo Troisi. Un film delizioso, in cui le diverse personalità dei due
      attori comici si completavano mitigando reciprocamente gli eccessi farseschi dell'uno e malinconici dell'altro.
      Seguirono alcuni film di successo in cui Benigni espresse la sua propria forma di comicità, decisamente poco
      raffinata, a volte becera, ma che ha dalla sua un gran numero di estimatori. Poi arrivò il colpo da maestro, il
      film “La vita è bella”, in cui Benigni affrontò il tema della Shoah per lanciarsi nella recitazione “impegnata”,
      pur mantenendo la vena prevalentemente comica. Grande successo per lo sfruttamento del <em>brand</em> Benigni,
      non a caso attraverso un premio Oscar a Hollywood, che ormai da qualche decennio suggella la morte dell'arte
      cinematografica sacrificata al mercato. Eppure i limiti dell'attore furono evidenti nei successivi flop di
      “Pinocchio” (il film più costoso della storia del cinema italiano) e “La tigre e la neve”; per non parlare del
      penoso ennesimo episodio de la Pantera Rosa. Il classico passo più lungo della gamba, dovuto alla non
      consapevolezza dei propri limiti. Occorreva un nuovo <em>format</em> per uscire dalla strada del declino, senza
      però tornare indietro alla comicità becera, che alla lunga stanca (e ha bisogno di volti sempre nuovi). Ecco così,
      messo da parte il cinema, arrivarono le letture di Dante, la vera trasformazione di Benigni: dal declino del
      comico ormai spompato alla sublimazione nel campo dell'alta cultura.</p>
    <p><a href="http://it.wikiquote.org/wiki/Sergio_Leone">Si racconta</a> che Sergio Leone dicesse che <em>“[Clint]
        Eastwood ... aveva solo due espressioni: con il cappello e senza cappello”</em>. Non so se il giudizio fosse
      appropriato (certamente non lo è per l'Eastwood più maturo), ma il Benigni di oggi è certamente peggio: lui ormai
      ha una sola espressione. Può parlare di Shoah, di guerra, di Inno di Mameli, di Divina Commedia, della
      Costituzione Italiana fino arrivare, nei giorni scorsi, ai Dieci Comandamenti, e tutte le cose appaiono nel
      medesimo modo. Non a caso un noto critico televisivo <a href="http://video.corriere.it/i-dieci-comandamenti-benigni-programma-geniale-ma-resta-qualche-perplessita/8ba204a8-8613-11e4-a2bf-0fba46a30b83">ha
        coniato</a>, a tal proposito, il termine “benignizzazione”. Fateci caso, Benigni non esce dal suo <em>cliché</em>
      neanche durante le interviste; come se Charlie Chaplin si fosse fatto vedere per tutta la vita, anche fuori dal <em>set</em>,
      con i baffetti, il cappello e il bastone. Qualsiasi cosa legga, la infarcisce degli stessi complimenti, iperboli,
      esclamazioni (spesso banali: <em>“ma che bello”</em>, <em>“questa è poesia”</em>, eccetera), mentre si dà una
      patina di finta umiltà con il suo atteggiamento da giullare. Apparentemente sfoggia magniloquenza, mentre la sua è
      solo ridondanza, una verbosità barocca. L'irresistible leggerezza della banalità che trionfa. <em>Che noia, che
        barba, che barba, che noia</em>...</p>
    <p>La retorica di Benigni è funzionale non all'esaltazione dell'argomento (Dante certo non ha bisogno delle sue
      lodi), ma alla magnificazione del declamatore; e al conseguente appagamento dell'ascoltatore, così fortunato da
      poter assistere al suo spettacolo. È un doppio fenomeno di luce riflessa: da Dante a Benigni, da Benigni
      all'ascoltatore. Alla fine è come se vi servissero un pranzo completo, dall'antipasto all'ammazzacaffè, in cui
      tutte le portate hanno lo stesso sapore dolciastro, servite dallo <em>chef</em> in persona sotto un effluvio di
      complimenti a sé stesso; voi però potrete dire di essere stati in quel ristorante così alla moda. Benigni è in
      effetti diventato come una multinazionale alimentare, capace di grandi profitti, grandi produzioni, grande <em>marketing</em>,
      grande distribuzione, ma che sforna prodotti sciapi e stereotipati; una specie di McDonald's della cultura, solo
      più caro.</p>
    <p>I benpensanti apprezzatori di Benigni, generalmente, attaccano le critiche negative tacciandole di elitismo: come
      se il problema fosse il fatto che un guitto, un giullare come Benigni si occupa di alta cultura. Forse c'è ancora
      qualche bacchettone che ragiona in questo modo; ma per quanto mi riguarda, l'osservazione è totalmente priva di
      significato. La contaminazione tra gli stili, tra il serio ed il faceto, tra il poetico e il comico, non è certo
      una novità e, solo a guardare l'ultimo secolo, ne abbiamo eccellenti dimostrazioni in attori-registi come Charlie
      Chaplin, Jacques Tati, Peter Sellers; per rimanere in Italia, Edoardo de Filippo, a suo modo Totò, fino ad
      arrivare al citato Massimo Troisi. Persino l'argomento più tragico, come la Shoah, può essere affrontato in chiave
      mista, come in <a href="http://en.wikipedia.org/wiki/Train_of_Life">“Train de Vie”</a> di Radu Mihăileanu - film
      che è ben più originale de “La vita è bella”. Non critico questa contaminazione; anzi, se fatta bene la apprezzo.
      Figuratevi che la mia passione per la Divina Commedia risale all'infanzia, alla lettura de <a href="http://it.wikipedia.org/wiki/L'Inferno_di_Topolino">“L'Inferno
        di Topolino”</a>, una parodia della Divina Commedia in salsa Disney, prodotta nel 1949 da due disegnatori
      italiani. Roba tipo: </p>
    <blockquote>
      <p>Io son nomato Pippo e son poeta<br />
        Or per l'inferno ce ne andremo a spasso<br />
        Verso oscura e dolorosa meta.</p>
    </blockquote>
    <p>Certo, cose da far inorridire i veri bacchettoni. Dalla vignetta in cui Topolino si addormenta e sogna di
      “entrare” nel libro attraverso un'illustrazione di Gustave Doré alla mia curiosità per la vera edizione della
      Commedia illustrata da Doré, che stava nella libreria in salotto, il passo fu breve. Con questo aneddoto è anche
      chiaro quale possa essere un valore pedagogico di una parodia; non a caso nelle strisce finali de “L'Inferno di
      Topolino” incontriamo il “vero” Dante, che mette a supplizio gli autori del fumetto, ma si persuade a liberarli
      dopo aver compreso che la loro opera può essere stimolo per la lettura dell'originale (ed è apprezzabile questa
      chiosa esplicita, vista la giovane età dei lettori). Dunque, l'avventura di Topolino nel fumetto è un sogno, è
      altro dall'originale, così pure la rocambolesca fuga di Schlomo e dei suoi compagni ebrei in “Train de Vie” è,
      purtroppo, un sogno, una fuga dalla tragica realtà, come viene rivelato nella scena finale. Solo uno sciocco
      lettore del fumetto può pensare che la Divina Commedia sia una storia di topi e paperi parlanti, così come solo
      uno sciocco spettatore del film può pensare che la Shoah fu un'allegra fuga in treno. Il rischio della
      banalizazione è evitato, né la pseudo-poesia o la pseudo-storia possono prendere il posto degli originali. Ben
      venga dunque la commistione tra stili, ben venga pure la parodia, anche da avanspettacolo - qualora non suoni
      irrispettosa - finché la rielaborazione rimane cosa diversa e distinguibile dal soggetto originale. </p>
    <p>Invece con Benigni la confusione è totale: l'attore non solo declama, ma diventa esegeta, costituzionalista,
      storico, teologo. Da attore a giullare, da giullare a declamatore di poesie, da declamatore a poeta, da poeta a
      grande intellettuale eclettico: basta procedere per piccoli passi e l'inganno è servito. Pazienza se - come nel
      caso dei Dieci Comandamenti - Benigni riduce il Cristianesimo a moralismo (come se lo scopo dei Comandamenti fosse
      solo la costruzione di un sistema etico), riporta cose in modo distorto, fino a falsificare dati storici (come
      quelli sull'incidenza della religione nelle guerre, contraddicendo quanto sostenuto da molti <a href="http://carm.org/religion-cause-war">studiosi</a>).
      Tanto l'evento televisivo è regolarmente preceduto e seguito da un'accurata campagna pubblicitaria,
      autoreferenziale, intrapresa dalla stessa RAI e dai suoi dirigenti, senza nessun confronto con opinioni critiche.
      È l'ennesima vittoria del pensiero unico, paradossalmente e tristemente interpretata da un ex anarchico ora
      imborghesito. Benigni non si cura neanche del principio di non contraddizione (francamente è inevitabile se ci si
      riduce all'elogio superlativo sempre e comunque): per cui quando parla dei Dieci Comandamenti li esalta sopra ogni
      cosa, mentre in un precedente spettacolo li aveva giudicati inferiori alla Costituzione Italiana (!) perché
      “pongono dei divieti”. Un'analisi sopraffina. Ma non è un problema, tanto la gente dimentica presto, della puntata
      precedente ricorda solo che Benigni è stato grande - e <em>deve</em> essere vero se è quello che sostengono
      (quasi) tutti. Nonostante le alte pretese, in realtà il prodotto di Benigni è paccottiglia, canzonette: in mente
      deve rimanere solo il ritornello. E così, mentre Benigni si arricchisce di qualche milione di euro a puntata, gli
      spettatori credono di raggiungere chissà quali vette culturali; invece diventano sempre più poveri in spirito. </p>
    <p>Certo, qualcuno forse si sentirà stimolato a leggere l'originale e questo sarebbe un effetto positivo, a
      prescindere dalla mediocrità della performance; ma la maggioranza, che già legge poco, si sentirà sazia di essere
      stata parte di un così grande evento e resterà convinta che la poesia nella Divina Commedia, o nei Dieci
      Comandamenti, sono quella parodia televisiva che ha messo in scena Benigni. E rimarrà privata a lungo, forse per
      sempre, della bellezza degli originali.</p>
    <p>PS Si dice che la performance di Benigni sui Dieci Comandamenti sia stata apprezzata dal Santo Padre: al momento
      in cui scrivo, la notizia non è verificabile (e la fonte è sospetta). Ma è vero che i giornali della corte papale,
      come Avvenire, hanno già incensato il comico toscano, mettendosi nella scia di tutti i conformisti. Non mi
      stupisce, anzi tutto torna, in un periodo storico in cui l'irresistibile leggerezza della banalità si è
      intrufolata da tempo anche nella Santa Sede.</p>
  </body>
</html>
